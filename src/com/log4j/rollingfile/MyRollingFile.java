/**
 * 
 */
package com.log4j.rollingfile;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author zhoufox
 *
 */
public class MyRollingFile {
	
	private static final Logger LOGGER = LogManager.getLogger(MyRollingFile.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (int i = 0; i < 2000; i++) {
			LOGGER.log(Level.ALL, "log ALL : HelloWorld");//制定Level类型的调用
			LOGGER.entry();
			LOGGER.trace("trace:HelloWorld");
			LOGGER.debug("debug:HelloWorld");
			LOGGER.info("info:HelloWorld");
			LOGGER.warn("warn:HelloWorld");
			LOGGER.error("error:HelloWorld");
			LOGGER.fatal("fatal:HelloWorld");
			LOGGER.log(Level.OFF, "log OFF : HelloWorld");//制定Level类型的调用
			LOGGER.exit();
		}
	}
}
