package com.log4j.file;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyFile {
	private static final Logger LOGGER = LogManager.getLogger(MyFile.class);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LOGGER.log(Level.ALL, "log ALL : HelloWorld");//制定Level类型的调用
		LOGGER.entry();
		LOGGER.trace("trace:HelloWorld");
		LOGGER.debug("debug:HelloWorld");
		LOGGER.info("info:HelloWorld");
		LOGGER.warn("warn:HelloWorld");
		LOGGER.error("error:HelloWorld");
		LOGGER.fatal("fatal:HelloWorld");
		LOGGER.log(Level.OFF, "log OFF : HelloWorld");//制定Level类型的调用
		LOGGER.exit();
	}
}
