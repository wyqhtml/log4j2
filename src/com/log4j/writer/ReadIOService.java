/**
 * 
 */
package com.log4j.writer;

import java.io.PipedReader;
import java.util.Scanner;

/**
 * @author zhoufox
 * 
 */
public class ReadIOService {

	PipedReader reader;

	public ReadIOService(PipedReader reader) {
		this.reader = reader;
	}

	public void run() {
		// 不间断地扫描输入流
		Scanner scanner = new Scanner(reader);

		// 将扫描到的字符流打印在屏目
		while (scanner.hasNext()) {
			System.out.println(scanner.nextLine());

			// 实现将字符流打印在GUI组件上 减少代码量 就不
		}
	}
}
