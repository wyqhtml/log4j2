写代码离不开bug,日志在测试的重要性显而易见。

在做项目的时候看到很多都是用log4j 我也不知道它好在哪里，大家用那就用它了，跟随大流。..@_@|||||..
用的时候感觉很简单，配置很强大，能想到的它都帮我做到了。没想到的它也能做。不用它我用谁？O__O"

###概念：

Logger：日志记录器，Log4j 允许开发人员定义多个Logger，每个Logger拥有自己的名字，Logger之间通过名字来表明隶属关系。有一个Logger称为Root，它永远存在，且不能通过名字检索或引用，可以通过Logger.getRootLogger()方法获得，其它Logger可以在类中通过 Logger.getLogger(String name)方法得到。
Appender：Appender则是用来指明将所有的log信息存放到什么地方，Log4j中支持多种appender
org.apache.log4j.ConsoleAppender(控制台)
org.apache.log4j.FileAppender(文件)
org.apache.log4j.DailyRollingFileAppender(每天产生一个日志文件)
org.apache.log4j.RollingFileAppender(文件大小到达指定尺寸的时候产生一个新的文件)
org.apache.log4j.WriterAppender(将日志信息以流格式发送到任意指定的地方) 
###还有很多输出方式没写出来（⊙ο⊙）之前我也不知道有那么多的输出方式，逗比了！！
Layout ：Layout的作用是控制Log信息的输出方式，也就是格式化输出的信息。
org.apache.log4j.HTMLLayout（以HTML表格形式布局）
org.apache.log4j.PatternLayout（可以灵活地指定布局模式）
以%开始，后面不同的参数代表不同的格式化信息
如："%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"
org.apache.log4j.SimpleLayout（包含日志信息的级别和信息字符串）
org.apache.log4j.TTCCLayout（包含日志产生的时间、线程、类别等等信息）
level是日志记录的优先级，分为OFF>FATAL>ERROR>WARN>INFO>DEBUG>TRACE> ALL等或者自己定义的级别
OFF：是最高等级的，用于关闭所有日志记录
FATAL：指出每个严重的错误事件将会导致应用程序的退出
Log4j建议只使用四个级别，优先级从高到低分别是ERROR、WARN、INFO、DEBUG
ALL：是最低等级的，用于打开所有日志记录
所有的log4j的级别，这些级别都相当于一层层的过滤器
Log4J 	TRACE 	DEBUG 	INFO 	WARN 	ERROR 	FATAL 
TRACE 	Y		Y		Y		Y		Y		Y
DEBUG 	N		Y		Y		Y		Y		Y
INFO 	N		N		Y		Y		Y		Y
WARN 	N		N		N		Y		Y		Y
ERROR 	N		N		N		N		Y		Y
FATAL 	N		N		N		N		N		Y
ALL 	Y		Y		Y		Y		Y		Y
OFF 	N		N		N		N		N		N
一个Logger可以拥有多个Appender，也就是你既可以将Log信息输出到屏幕，同时存储到一个文件中。
###准备阶段：


先到官网下载log4j支持包
http://logging.apache.org/log4j/2.x/download.html

导入log4j-api-2.3.jar，log4j-core-2.3.jar两个包到项目中，有这两个包就可以运行了
如果要将log用流的方式写出去还要加上log4j-iostreams-2.3.jar
其他的包不了解，还是不要加上去了，听说会有干扰，不知是真的还是假的。

###开始放干货了：

先来个最常用的控制台打印

##注意：

在导包的时候注意了，是导这两个包
````
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
```
一开始的时候我没注意，导错了包(—_—+)，在getLogger(MyConsole.class);总是出错,就是因为导了java自带的log工具包
```
import java.util.logging.LogManager;
import java.util.logging.Logger;
```
或者是导成了
```
import org.apache.logging.log4j.core.Logger;
```
就差core一个单词，不留意看真的很容易导出包啊！（>"<）``

LogManager.getLogger(MyConsole.class);
或者是LogManager.getLogger(MyConsole.class.getName());都可以
在我看别人的代码的时候我发现怎么一个是class一个是name
在这里我纠结了不知道是写class好点还是写name更快……-_-"
##MyConsole.java
```
package com.log4j.console;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * @author zhoufox
 * 
 */
public class MyConsole {
	private static final Logger LOGGER = LogManager.getLogger(MyConsole.class);
	public static void main(String[] args) {
		LOGGER.log(Level.ALL, "log ALL : HelloWorld");//制定Level类型的调用
		LOGGER.entry();
		LOGGER.trace("trace:HelloWorld");
		LOGGER.debug("debug:HelloWorld");
		LOGGER.info("info:HelloWorld");
		LOGGER.warn("warn:HelloWorld");
		LOGGER.error("error:HelloWorld");
		LOGGER.fatal("fatal:HelloWorld");
		LOGGER.log(Level.OFF, "log OFF : HelloWorld");//制定Level类型的调用
		LOGGER.exit();
	}
}
```

就这样可以运行了，也能打印，只是没有打印出全部信息。有部分信息被打印出来了，还有部分没有被打印。
因为还没配置它，它只有用默认的配置打印而已。
写配置的时候比较坑爹的是，我用的是2.3版本，而我看的代码，网上找的教程大部分都是1.X版本的它们不适用于2.X 有很多的改变。
导致我写好log4j.xml放到src目录下程序一直告诉我说没找到配置它自己用默认的配置帮我打印出来。很着狂（*>.<*）
后来看了天外的星星的博客才知道原来1.X和2.X有很多的差别 
其配置文件只能采用.xml, .json或者 .jsn。没有了.properties文件
名字改成log4j2.xml了
xml节点名改了，用法也改了
对此我感到很陌生，我以为会有默认配置文件，但我找遍了bin目录下的jar包都没有找到xml配置文件，还逗比⊙﹏⊙‖∣°的去apache官网下了src文件也没找的，不过也有点收获：我找到了test文件里面有很详细的log4j的测试文件，但表示看不懂(～ o ～)~zZ 

发挥百度大婶的威力，找了N个XXX博客,XXX文章拼凑起来的log4j2.xml
还没写完下次再写
2015-09-28 02:36:09

在写流输出的时候，找资料不小心点进了
http://logging.apache.org/log4j/2.x/manual/appenders.html
看到了n个输出方式。之前我就觉得log4j很叼了，没想到它更吊。还是井底之蛙啊！眼界太小了，我的发粪涂墙了。
2015-10-01 23:32:51